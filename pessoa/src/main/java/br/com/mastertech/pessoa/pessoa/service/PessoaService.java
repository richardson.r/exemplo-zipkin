package br.com.mastertech.pessoa.pessoa.service;

import br.com.mastertech.pessoa.pessoa.client.Endereco;
import br.com.mastertech.pessoa.pessoa.client.EnderecoClient;
import br.com.mastertech.pessoa.pessoa.dto.PessoaDTO;
import br.com.mastertech.pessoa.pessoa.exception.PessoaNaoEncontradaException;
import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import br.com.mastertech.pessoa.pessoa.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private EnderecoClient enderecoClient;

    public Pessoa salvar(Pessoa pessoa){
        return pessoaRepository.save(pessoa);
    }

    public Pessoa getById(Long id){
        return pessoaRepository.findById(id).orElseThrow(PessoaNaoEncontradaException::new);
    }

    public Pessoa salvar(PessoaDTO pessoaDTO){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(pessoaDTO.getNome());
        pessoa.setCpf(pessoaDTO.getCpf());

        Endereco endereco = new Endereco();
        endereco = enderecoClient.getByCep(pessoaDTO.getCep());
        pessoa.setEnderecoId(endereco.getId());

        return pessoaRepository.save(pessoa);
    }
}
