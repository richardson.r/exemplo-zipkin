package br.com.mastertech.pessoa.pessoa.controller;

import br.com.mastertech.pessoa.pessoa.client.EnderecoClient;
import br.com.mastertech.pessoa.pessoa.dto.PessoaDTO;
import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import br.com.mastertech.pessoa.pessoa.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("/pessoa")
public class PessoaControler {

    @Autowired
    private EnderecoClient enderecoClient;

    @Autowired
    private PessoaService pessoaService;

    @GetMapping("/{id}")
    public Pessoa getById(@PathVariable Long id) {
        return pessoaService.getById(id);
    }

    @PostMapping
    public Pessoa create(@RequestBody PessoaDTO pessoaDTO){
        return pessoaService.salvar(pessoaDTO);
    }


}
