package br.com.mastertech.pessoa.pessoa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Pessoa não encontrada")
public class PessoaNaoEncontradaException extends RuntimeException {
}
