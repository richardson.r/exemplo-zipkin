package br.com.mastertech.pessoa.pessoa.repository;

import br.com.mastertech.pessoa.pessoa.model.Pessoa;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {
}
