package br.com.mastertech.endereco.service;

import br.com.mastertech.endereco.client.Cep;
import br.com.mastertech.endereco.client.CepClient;
import br.com.mastertech.endereco.exception.EnderecoNotFoundException;
import br.com.mastertech.endereco.models.Endereco;
import br.com.mastertech.endereco.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private CepClient cepClient;

    public Endereco create(Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    public Endereco getByCep(String cep) {
        Optional<Endereco> enderecoOptional = enderecoRepository.findByCep(cep);

        if (enderecoOptional.isPresent()) {
            return enderecoOptional.get();
        } else {
            Cep cepWs = new Cep();
            cepWs = cepClient.getCep(cep);

            if (cepWs.getLogradouro() == null) {
                throw new EnderecoNotFoundException();

            } else {
                Endereco endereco = new Endereco();
                endereco.setCep(cep);
                endereco.setLogradouro(cepWs.getLogradouro());
                endereco.setLocalidade(cepWs.getLocalidade());

                endereco = enderecoRepository.save(endereco);
                return endereco;
            }
        }
    }

    public Cep create(String cep) {
        return cepClient.getCep(cep);
    }
}
