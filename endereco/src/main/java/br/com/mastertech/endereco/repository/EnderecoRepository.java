package br.com.mastertech.endereco.repository;

import br.com.mastertech.endereco.models.Endereco;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EnderecoRepository extends CrudRepository<Endereco, String> {
    Optional<Endereco> findByCep(String cep);
}
