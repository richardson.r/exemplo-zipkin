package br.com.mastertech.endereco.controller;

import br.com.mastertech.endereco.models.Endereco;
import br.com.mastertech.endereco.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EnderecoController {

    @Autowired
    private EnderecoService enderecoService;

    @PostMapping("/{cep}/salvar")
    public Endereco create(@RequestBody Endereco endereco) {
        return enderecoService.create(endereco);
    }

    @GetMapping("/{cep}")
    public Endereco getByCep(@PathVariable String cep) {
        return enderecoService.getByCep(cep);
    }

}
